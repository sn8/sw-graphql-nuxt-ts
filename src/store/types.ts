export interface RootState {
  isLoading: boolean;
  list: Array<{}>;
  autocompleteList: Array<{}>;
}