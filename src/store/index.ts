import Vue from 'vue'
import Vuex, { StoreOptions, MutationTree } from 'vuex'
import gql from 'graphql-tag'
import graphqlClient from '@/utils/graphql-client'
import { RootState } from './types'

Vue.use(Vuex)

const state = {
  isLoading: false,
  list: [],
  autocompleteList: []
}

const actions = {
  async getPeoples({ commit }, name) {
    commit('startLoading')
    try {
      const response = await graphqlClient.query({
        query: gql`
          query People($name: String) {
            peoples(name: $name) {
              name
              birth_year
              eye_color
              height
              weight
              species
              homeworld
            }
          }
        `,
        variables: { name }
      })

      commit('setList', response.data.peoples)
    } finally {
      commit('endLoading')
    }
  },

  async getAutocompleteItems({ commit }, name) {
    const response = await graphqlClient.query({
      query: gql`
        query People($name: String) {
          autocomplete(name: $name) {
            name
          }
        }
      `,
      variables: { name }
    })

    commit('setAutocompleteList', response.data.autocomplete)
  }
}

const mutations: MutationTree<RootState> = {
  setList(state, list) {
    state.list = list
  },

  setAutocompleteList(state, list) {
    state.autocompleteList = list
  },

  startLoading(state) {
    state.isLoading = true
  },

  endLoading(state) {
    state.isLoading = false
  }
}

const storeOptions: StoreOptions<RootState> = {
  state,
  actions,
  mutations
}

const store = () => new Vuex.Store<RootState>(storeOptions)

export default store
